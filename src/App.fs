﻿module FSharp_Muenster_Projekt

open Elmish
open Elmish.React
open Elmish.HMR
open Fable.React
open Fable.React.Props

Fable.Core.JsInterop.importSideEffects "./index.css"


// Kalenderansicht der aktuellen Woche
// Bereiche sperren
// Termine vorschlagen (ausser in gesperrten Bereichen)

type Zeitbereich =
  {
    Beginn : System.DateTime
    Ende : System.DateTime
  }

type Termin =
  {
    Titel : string
    Zeitbereich : Zeitbereich
  }

type Model = 
  {
    Kalender_Woche : uint
    Tage : System.DateTime list // DateTime kein schöner Datentyp
    Gesperrte_Tage : System.DateTime list // Muss Minutenganaue Zeitbereiche einschließen können
    Vorgeschlagene_Termine : Termin list
  }
  // Datum basiert 
  // Timeslot / Zeitbereich
  // Tage / Uhrzeiten (HH:MM)



type Msg =
  | Sperre of System.DateTime
  | Entsperre of System.DateTime
  

let Montag = System.DateTime.Parse "05/30/2022"
let Dienstag = System.DateTime.Parse "05/31/2022"
let Mittwoch = System.DateTime.Parse "06/01/2022"
let Donnerstag = System.DateTime.Parse "06/02/2022"
let Freitag = System.DateTime.Parse "06/03/2022"
let Samstag = System.DateTime.Parse "06/04/2022"
let Sonntag = System.DateTime.Parse "06/05/2022"


let init () : Model*Cmd<Msg> = 
  ({
    Kalender_Woche = 23u
    Tage = [ Montag ; Dienstag ; Mittwoch ; Donnerstag; Freitag; Samstag; Sonntag ]
    Gesperrte_Tage = []
    Vorgeschlagene_Termine = []
  }, Cmd.none )


let update (msg: Msg) (model : Model) : Model*Cmd<Msg> =
  match msg with
  | Sperre tag -> 
      {
         model with Gesperrte_Tage = [ tag ]
      }, Cmd.none
 
  | Entsperre tag -> 
      {
         model with Gesperrte_Tage = [ ]
      }, Cmd.none
 

let kalenderwoche kw =
  div [ Class "text-center font-bold text-4xl" ]
    [
      sprintf "Kalenderwoche %i" kw
      |> ofString
      
    ]

let wochentag (tag : System.DateTime) = 
  match tag.Date.DayOfWeek with
  | System.DayOfWeek.Monday -> "Mo" |> ofString 
  | System.DayOfWeek.Tuesday -> "Di" |> ofString
  | System.DayOfWeek.Wednesday -> "Mi" |> ofString
  | System.DayOfWeek.Thursday -> "Do" |> ofString
  | System.DayOfWeek.Friday -> "Fr" |> ofString
  | System.DayOfWeek.Saturday-> "Sa" |> ofString
  | System.DayOfWeek.Sunday -> "So" |> ofString


let wochentag_anzeige dispatch (gesperrte_Tage : System.DateTime list) tag = 
  let bg_color = 
    if gesperrte_Tage |> List.contains tag then "bg-red-400" else "bg-white"
  div [ Class  $"w-16 h-16 cursor-pointer {bg_color} hover:bg-gray-200 border-2 text-center font-bold text-2xl"
        OnClick (fun _ -> dispatch (Sperre tag))
      ] 
    [ wochentag tag ]


let wochentage dispatch  (wochentage : System.DateTime list) (gesperrte_Tage : System.DateTime list) = 
  div [ Class "flex space-x-2 border-2" ]
    ( wochentage
      |> List.map (wochentag_anzeige dispatch gesperrte_Tage) )
          

let view (model : Model) dispatch =
  section [ Class "w-full"]
    [
      kalenderwoche model.Kalender_Woche
      
      wochentage dispatch model.Tage model.Gesperrte_Tage
      
      // Tag  sperren können 


      // Gesperrten und Gebuchten Termine

      // Termin Eintragen können
      
      ]


Program.mkProgram init update view
#if DEBUG
|> Program.withConsoleTrace
#endif
|> Program.withReactSynchronous "root"
|> Program.run